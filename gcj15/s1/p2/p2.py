def divide(n):
  if n%2==0:
    return [n/2, n/2]
  else:
    return [(n/2)+1, n/2]
cases = int(raw_input())
for z in xrange(1, cases+1):
  d = int(raw_input())
  p = map(int, raw_input().split())
  s = {x:p.count(x) for x in p}
  k = [0] + sorted(s.keys())
  c = 0
  a = k[-1]
  while k[-1]>1:
    #print c, a, s, k
    if(a>(c+k[-1])):
      a = c+k[-1]
    m = k.pop()
    n = divide(m)
    c += s[m]
    k = sorted(list(set(k + n)))
    for i in n:
      if i in s:
        s[i] += 1*s[m]
      else:
        s[i] = 1*s[m]
    del s[m]
  print "Case #" + str(z) + ": " + str(a)
