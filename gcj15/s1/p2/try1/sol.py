cases = int(raw_input())
for j in xrange(1, cases+1):
  d = int(raw_input())
  p = map(int, raw_input().split())
  c = 0
  bw =0
  w = [0]
  for i in p:
    if i > 3:
      w.append(i)
    elif i > c:
      c = i
  sc = c
  mt = max(w)
  while len(w) > 1:
    m = max(w)
    if mt > m+c+bw-sc:
      mt = m+c+bw-sc
    if mt < c+bw:
      c = mt
      bw= 0
      break
    else:
      c+=1
      w.remove(m)
      if m < 7:
        if m > 4 and sc<3 and bw<3-sc:
          bw = 3-sc
        elif sc <2 and bw<2-sc:
          bw = 2-sc
      else:
        if m == 7:
          w += [4]
        else:
          if m%2==0:
            w += [m/2, m/2]
          else:
            w += [int(m/2), int(m/2)+1]
  c += bw
  print "Case #" + str(j) + ": " + str(c)
