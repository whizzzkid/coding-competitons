cases = int(raw_input())
for j in xrange(1, cases+1):
  d = int(raw_input())
  p = map(int, raw_input().split())
  print " ".join(map(str, p))
  c = 0
  b = 0
  if 3 in p:
    b = 3
  elif 2 in p:
    b = 2
  elif 1 in p:
    b = 1
  m = max(p)
  mt = m
  while c+b < mt and m > 3:
    if mt > c + m:
      mt = c + m
    print b, c, m, mt, p
    if mt < c + b:
      c = mt
      b = 0
      break
    else:
      c += 1
      p.remove(m)
      if m < 7:
        if m > 4 and b<3:
          b =3
        elif m >2 and b<2:
          b = 2
        elif b < 1:
          b = 1
      else:
        if m == 7:
          if b != 3:
            b = 3
          p += [4]
        else:
          if m%2 ==0:
            p += [m/2, m/2]
          else:
            p += [int(m/2), int(m/2)+1]
    if len(p) >1:
      m = max(p)
    else:
      m = 0
  print "Case #" + str(j) + ": " + str(min(c+b, mt))
