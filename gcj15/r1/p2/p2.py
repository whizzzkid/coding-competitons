# Problem solving template

# Reversing a number.
def reverse(n):
    return int(str(num)[::-1])

# Frequency Count
def freq(a):
    return {x:a.count(x) for x in a}

def odd_fill1(r, c, t):
    uh = 0
    rby2 = r/2
    rby21 = (r/2)+1
    cby2 = c/2
    cby21 = (c/2)+1
    t -= rby21 * cby21
    #print rby2, cby2, rby21, cby21
    for i in xrange((rby2*cby21) + (cby2*rby21)):
        t-=1
        uh+=2
        if t==0: break
    while t>0:
        t-=1
        uh+=4
    return uh

def odd_fill2(r, c, t):
    uh = 0
    rby2 = r/2
    rby21 = (r/2)+1
    cby2 = c/2
    cby21 = (c/2)+1
    t -= ((r*c)/2)+1
    for i in xrange(2*(rby2+cby2)):
        t-=1
        uh+=3
        if t==0: break
    while t>0:
        t-=1
        uh+=4
    return uh

def solve(r,c,t):
    uh = 0
    if t == 0:
        return uh
    else:
        grid = r*c
        wall = (r*(c-1)) + (c*(r-1))
        if t == grid:
            return wall
        else:
            if grid%2:
                # odd
                if t <= (grid/2)+1:
                    return uh
                else:
                    return min(odd_fill1(r,c,t), odd_fill2(r,c,t))
            else:
                # even
                if t <= (grid/2):
                    return uh
                else:
                    t -= (grid/2)
                    for i in xrange(2):
                        t-=1
                        uh+= 2
                        if t==0: break
                    for i in xrange(r+c-4):
                        t-=1
                        uh+=3
                        if t==0: break
                    while t>0:
                        t-=1
                        uh+=3
                    return uh

cases=int(raw_input())
for rec in xrange(1, cases+1):
    r,c,t = map(int, raw_input().split())
    print "Case #" + str(rec) + ': ' + str(solve(r,c,t))
