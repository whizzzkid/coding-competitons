import sys
sys.setrecursionlimit(100000000)
PP
# Reversing a number.
def reverse(num):
    return int(str(num)[::-1])

# Frequency Count
def freq(a):
    return {x:a.count(x) for x in a}

def countNum(num, count=0):
    #print num, count
    if num <= 19:
        return count + num
    else:
        newnum = num-1
        rev = reverse(num)
        if ((num % 10) == 0) or (rev >= num):
            return countNum(newnum, count+1)
        else:
            return countNum(rev, count+1)

cases = int(raw_input())
for rec in xrange(1, cases+1):
    print "Case #" + str(rec) + ': ' + str(countNum(int(raw_input())))
