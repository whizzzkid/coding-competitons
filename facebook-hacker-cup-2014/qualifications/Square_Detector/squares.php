<?php
/**
 * Solution to Square Detector Problem
 * @author: whizzzkid
 */
/**
 * Reading File
 */
$ip = fopen('square_detector_sample_input_2.txt', 'r');
$op = fopen('square_detector_sample_output_2.txt', 'w');
$T = trim(fgets($ip));
$count = 0;
while($count < $T){
  $N = trim(fgets($ip));
  // Making a pixel array of black. Of Format:
  // pixel[n][x] = X
  // pixel[n][y] = Y
  $pixel = array();
  for($i=0;$i<$N;$i++){
    $line = trim(fgets($ip));
    for($j=0;$j<strlen($line);$j++){
      if($line[$j] == '#'){
        $pixel[] = array('x' => $j, 'y'=> $i);
      }
    }
  }
  // Answer by default NO
  $answer = 'NO';
  $squareSide = sqrt(count($pixel));
  // Can total pixels make a square?
  if(ctype_digit((string)$squareSide) && $squareSide != 0.0){
    // first pixel is top left pixel
    $topLeft = $pixel[0];
    // calculating the furthest pixel
    $bottomRight['x'] = $topLeft['x'] + $squareSide -1;
    $bottomRight['y'] = $topLeft['y'] + $squareSide -1;
    // checking if furthest is out of bounds.
    if($bottomRight['x']<=$N && $bottomRight['y']<=$N){
      $answer = 'YES';
      // chexking if each pixel is in bounds of the square.
      foreach($pixel as $p){
        if($p['x'] < $topLeft['x'] || $p['x'] > $bottomRight['x']){
          $answer = 'NO';
          break;
        }
        if($p['y'] < $topLeft['y'] || $p['y'] > $bottomRight['y']){
          $answer = 'NO';
          break;
        }
      }
    }
  }
  $count++;
  fwrite($op, sprintf("Case #%d: %s\n", $count, $answer));
}
