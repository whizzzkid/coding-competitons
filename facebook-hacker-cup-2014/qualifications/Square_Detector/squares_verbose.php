<?php
/**
 * Solution to Square Detector Problem
 * @author: whizzzkid
 * Note: Run in it in the browser to see the verbose output.
 */
$ip = fopen('square_detector.txt', 'r');
$op = fopen('square_detector_output.txt', 'w');
$T = trim(fgets($ip));
$count = 0;
while($count < $T){
  $N = trim(fgets($ip));
  $pixel = array();
  for($i=0;$i<$N;$i++){
    $line = trim(fgets($ip));
    for($j=0;$j<strlen($line);$j++){
      if($line[$j] == '#'){
        $pixel[] = array('x' => $j, 'y'=> $i);
      }
    }
  }
  $answer = 'NO';
  $squareSide = sqrt(count($pixel));
  echo 'Test Case '.$count.' square side: '.$squareSide.'<br>';
  if(ctype_digit((string)$squareSide) && $squareSide != 0.0){
    $topLeft = $pixel[0];
    $bottomRight['x'] = $topLeft['x'] + $squareSide -1;
    $bottomRight['y'] = $topLeft['y'] + $squareSide -1;
    echo '['.$topLeft['x'].','.$topLeft['y'].'] to ['.$bottomRight['x'].','.$bottomRight['y'].'] <br>';
    if($bottomRight['x']<=$N && $bottomRight['y']<=$N){
      $answer = 'YES';
      foreach($pixel as $p){
        echo '['.$p['x'].','.$p['y'].'] :';
        if($p['x'] < $topLeft['x'] || $p['x'] > $bottomRight['x']){
          $answer = 'NO';
          echo 'FAIL<br>';
          break;
        }
        if($p['y'] < $topLeft['y'] || $p['y'] > $bottomRight['y']){
          $answer = 'NO';
          echo 'FAIL<br>';
          break;
        }
        echo 'OK<br>';
      }
    }else{
      echo 'Square Out Of Bounds<br>';
    }
  }
  $count++;
  fwrite($op, sprintf("Case #%d: %s\n", $count, $answer));
}
