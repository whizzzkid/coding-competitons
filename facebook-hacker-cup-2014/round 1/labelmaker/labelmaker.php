<?php
/**
 * Solution to Label Maker Problem
 * @author: whizzzkid
 */
/**
 * Reading File
 */
$ip                 = fopen('labelmaker_example_input.txt', 'r');
//$op = fopen('square_detector_sample_output_2.txt', 'w');
$T                  = trim(fgets($ip));
$count              = 0;
while($count < $T){
  $case             = explode(" ",trim(fgets($ip)));
  $testString       = (string)$case[0];
  $boxCount         = (int)$case[1];
  $stringLengthCopy = strlen($testString);
  $stringLength     = $stringLengthCopy;
  for ($len=1; $boxCount > $stringLength ; $len++) {
    $boxCount       = $boxCount - $stringLength;
    $stringLength   = $stringLength * $stringLengthCopy;
    //echo $boxCount." ".$stringLength."<br>";
  }
  //echo $len;
  //echo "<br>";
  $ret              = "";
  for ($i=1 ; $i <= $len ; $i++) {
    $stringLength   = $stringLength / $stringLengthCopy;
    echo $stringLength. " ". $boxCount. " <br>";
    for ($c=0 ; $c < $stringLengthCopy && $boxCount > $stringLength ; $c++) {
      $boxCount     = $boxCount - $stringLength;
    }
    echo $stringLength. " ". $boxCount. " ". $c.'<br>';
    $ret            = $ret . $testString[$c];
  }
  echo $ret.'<br>';
  $count++;
  //fwrite($op, sprintf("Case #%d: %s\n", $count, $answer));
}
