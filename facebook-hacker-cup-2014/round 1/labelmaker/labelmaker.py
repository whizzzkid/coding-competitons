# Solution to Label Maker Problem
# @author: whizzzkid
cases = int(raw_input())
for case in range(cases):
  testCase = raw_input().split();
  testString = str(testCase[0])
  boxCount = int(testCase[1])
  stringLengthCopy = len(testString)
  stringLength = stringLengthCopy
  length = 1
  print length, boxCount, stringLength
  while boxCount > stringLength:
    length = length + 1
    boxCount = boxCount - stringLength;
    stringLength = stringLength * stringLengthCopy;
    print length, boxCount, stringLength
  answer = ""
  for i in range(length):
    stringLength = stringLength / stringLengthCopy
    c = 0
    while (c < stringLengthCopy) and (boxCount > stringLength):
      boxCount = boxCount - stringLength
      c = c + 1
    answer = answer + testString[c]
    print length, boxCount, stringLength, c
  print "Case #" + str(case + 1) + ": " + answer
