# Solution to Coins Game Problem
# @author: whizzzkid
cases = int(raw_input())
for case in range(cases):
  n,k,c = map(int, raw_input().split())
  answer = c
  if n>k:
    answer += (n%k)
  if c==k:
    answer += (k%n)
  print "Case #" + str(case + 1) + ": " + str(answer)
