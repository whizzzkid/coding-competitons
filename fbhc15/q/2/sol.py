# FB hacker cup 2015 q2
from itertools import groupby

def verify_food_list(foods, req):
  p=c=f=0
  for i in foods:
    p += i['p']
    c += i['c']
    f += i['f']
  if p == req['p'] and c == req['c'] and f == req['f']:
    return True
  return False

def findsubsets(S,K):
  ans = []
  if K in S:
    ans += [[K]]
  for i,s in enumerate(S):
    if K-s>0:
      d = map(lambda x: [s]+x,findsubsets(S[:i]+S[i+1:],K-s))
      if len(d):	ans += d
  return ans

def find_best_food(foodts,req,foods):
  f = [sorted(j) for j in findsubsets(foodts,req['t'])]
  f.sort()
  res = list()
  for e in [key for key,g in groupby(f)]:
    for i in e:

cases = int(raw_input())
for i in xrange(cases):
  req = dict()
  req['p'], req['c'], req['f'] = map(int, raw_input().split())
  req['t'] = req['p'] + req['c'] + req['f']
  itmcnt = int(raw_input())
  foods = list()
  foodt = 0
  foodts= list()
  for j in xrange(itmcnt):
    itm = dict()
    itm['p'], itm['c'], itm['f'] = map(int, raw_input().split())
    itm['t'] = itm['p'] + itm['c'] + itm['f']
    foodt += itm['t']
    foodts.append(itm['t'])
    foods.append(itm)
  foods.sort(key=lambda x: x['t'])
  if foodt >= req['t']:
    print find_best_food(foodts, req, foods)
  else:
    print 'no'
